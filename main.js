"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var boat_1 = require("./models/boat");
var car_1 = require("./models/car");
var plane_1 = require("./models/plane");
var boat1 = new boat_1.Boat("Zephyr", 20);
var boat2 = new boat_1.Boat("Sapphire", 30);
var car1 = new car_1.Car("Lamborghini Diablo", 40);
var car2 = new car_1.Car("Ferrari Testarossa", 50);
var plane1 = new plane_1.Plane("Hawker Hurricane", 200);
var plane2 = new plane_1.Plane("Tupolev", 4);
var vehicles = [boat1, boat2, car1, car2, plane1, plane2];
var moveVehicles = function () {
    vehicles.forEach(function (vehicle) {
        for (var i = 0; i < 2; i++) {
            vehicle.accelerate();
        }
        for (var i = 0; i < 10; i++) {
            vehicle.decelerate();
        }
    });
};
var vehiclesByType = function (Class) {
    return vehicles.filter(function (vehicle) {
        if (vehicle instanceof Class) {
            return vehicle;
        }
    });
};
moveVehicles();
vehiclesByType(boat_1.Boat).forEach(function (vehicle) {
    console.log("Vehicle with name ".concat(vehicle.name, " has a speed of ").concat(vehicle.currentSpeed, " km/h"));
});
