"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Plane = void 0;
var vehicle_1 = require("./vehicle");
var Plane = /** @class */ (function (_super) {
    __extends(Plane, _super);
    function Plane(name, currentSpeed) {
        var _this = _super.call(this) || this;
        _this._name = name;
        if (currentSpeed > 0 && currentSpeed < 200) {
            _this._currentSpeed = currentSpeed;
        }
        else {
            currentSpeed < 0 ? (_this._currentSpeed = 0) : (_this._currentSpeed = 200);
        }
        return _this;
    }
    return Plane;
}(vehicle_1.Vehicle));
exports.Plane = Plane;
