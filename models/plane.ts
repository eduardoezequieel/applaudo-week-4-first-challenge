import { Vehicle } from "./vehicle";

export class Plane extends Vehicle {
  constructor(name: string, currentSpeed: number) {
    super();

    this._name = name;

    if (currentSpeed > 0 && currentSpeed < 200) {
      this._currentSpeed = currentSpeed;
    } else {
      currentSpeed < 0 ? (this._currentSpeed = 0) : (this._currentSpeed = 200);
    }
  }
}
