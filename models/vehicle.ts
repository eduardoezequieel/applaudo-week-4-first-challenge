export abstract class Vehicle {
  protected _name: string;
  protected _currentSpeed: number;

  get name() {
    return this._name;
  }

  get currentSpeed() {
    return this._currentSpeed;
  }

  accelerate() {
    this._currentSpeed > 0 && this._currentSpeed < 200
      ? this._currentSpeed++
      : (this._currentSpeed = this._currentSpeed);
  }

  decelerate() {
    this._currentSpeed >= 1 && this._currentSpeed <= 200
      ? this._currentSpeed--
      : (this._currentSpeed = this._currentSpeed);
  }
}
