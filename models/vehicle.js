"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Vehicle = void 0;
var Vehicle = /** @class */ (function () {
    function Vehicle() {
    }
    Object.defineProperty(Vehicle.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Vehicle.prototype, "currentSpeed", {
        get: function () {
            return this._currentSpeed;
        },
        enumerable: false,
        configurable: true
    });
    Vehicle.prototype.accelerate = function () {
        this._currentSpeed > 0 && this._currentSpeed < 200
            ? this._currentSpeed++
            : (this._currentSpeed = this._currentSpeed);
    };
    Vehicle.prototype.decelerate = function () {
        this._currentSpeed >= 1 && this._currentSpeed <= 200
            ? this._currentSpeed--
            : (this._currentSpeed = this._currentSpeed);
    };
    return Vehicle;
}());
exports.Vehicle = Vehicle;
