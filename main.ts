import { Boat } from "./models/boat";
import { Car } from "./models/car";
import { Plane } from "./models/plane";
import { Vehicle } from "./models/vehicle";

const boat1 = new Boat("Zephyr", 20);
const boat2 = new Boat("Sapphire", 30);

const car1 = new Car("Lamborghini Diablo", 40);
const car2 = new Car("Ferrari Testarossa", 50);

const plane1 = new Plane("Hawker Hurricane", 200);
const plane2 = new Plane("Tupolev", 4);

const vehicles = [boat1, boat2, car1, car2, plane1, plane2];

const moveVehicles = () => {
  vehicles.forEach((vehicle) => {
    for (let i = 0; i < 2; i++) {
      vehicle.accelerate();
    }

    for (let i = 0; i < 10; i++) {
      vehicle.decelerate();
    }
  });
};

const vehiclesByType = (Class) => {
  return vehicles.filter((vehicle) => {
    if (vehicle instanceof Class) {
      return vehicle;
    }
  });
};

moveVehicles();

vehiclesByType(Vehicle).forEach((vehicle) => {
  console.log(
    `Vehicle with name ${vehicle.name} has a speed of ${vehicle.currentSpeed} km/h`
  );
});
